//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct distance {
   float x1,x2,y1,y2,result;
};  
float input_values();
int main()
{
    struct distance d1;
    d1.x1=input_values();
    d1.x2=input_values();
    d1.y1=input_values();
    d1.y2=input_values();
    d1.result=sqrt( pow((d1.x1-d1.x2),2) + pow((d1.y1-d1.y2), 2) );
    printf("THE DISTANCE BETWEEN 2 POINTS=%f",d1.result);
    return 0;
}
float input_values()
{
    float x;
    printf("ENTER THE VALUES OF THE COORDINATES=");
    scanf("%f",&x);
    return(x);
    
    
}