#include<stdio.h>
float input();
float print_result(float r);
float sum(float n1,float n2);

int main()
{
   float A,B,result;
   printf("ENTER VALUES OF A,B\n");
   A=input();
   B=input();
   result=sum(A,B);
   print_result(result);
   return 0;
}
// function to input the number
float input()
{   
  
   float x;
   scanf("%f",&x);
   return(x);
   
}
// function to print sum
float print_result(float r)
{ 
    printf("SUM OF 2 NUMBERS=%0.4f",r);     
}
// function to calculate the sum of 2 number
float sum(float n1,float n2)
{   
    
    return(n1+n2);
    
}
