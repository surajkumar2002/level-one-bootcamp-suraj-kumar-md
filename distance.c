#include<stdio.h>
#include<math.h>
float input_values(int c);
void print_result(float x1,float x2,float y1,float y2,float r);
float dist(float x1,float y1,float x2,float y2);

int main()
{  float x1,y1,x2,y2;
   x1=input_values(1);
   y1=input_values(2);
   x2=input_values(3);
   y2=input_values(4);
   float result=dist(x1,y1,x2,y2);
   print_result(x1,y1,x2,y2,result);
   return 0;
}
// function to input the coordinates
float input_values(int c)
{   
  
   
   float x1,y1,x2,y2;
   if(c==1)
   {
       printf("Enter the values of x%d:",c);
       scanf("%f",&x1);
   }
   else if(c==2)
   {
       printf("Enter the values of y%d:",c-1);
       scanf("%f",&y1);
   }
   else if(c==3)
   {
       printf("Enter the values of x%d:",c-1);
       scanf("%f",&x2);
   }
   else
   {
        printf("Enter the values of y%d:",c-2);
       scanf("%f",&y2);
   }
   
}
// function to print distance
void print_result(float x1,float x2,float y1,float y2,float r)
{ 
    printf("DISTANCE BETWEEN POINTS (%0.2f,%0.2f) AND (%0.2f,%0.2f)=%0.4f",x1,y1,x2,y2,r);     
}
// function to calculate the distanc between 2 points
float dist(float x1,float y1,float x2,float y2)
{   
    
    return((sqrt( pow((x1-x2),2) + pow((y1-y2), 2) )));
    
}
